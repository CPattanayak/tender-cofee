package com.cofee.common.implimenter;

import com.cofee.common.Currency;
import com.cofee.common.DispenseCurrencyChain;

public class Currency5Dispenser implements DispenseCurrencyChain {

	private DispenseCurrencyChain chain;
	int count=0;
	
	@Override
	public void setNextChain(DispenseCurrencyChain nextChain) {
		this.chain=nextChain;
	}

	@Override
	public void dispense(Currency cur) {
		if(cur.getAmount() >= 5){
			int num = cur.getAmount()/5;
			if(num>count)throw new AvalableBalanceException("Avalable50 balance notthere");
			int remainder = cur.getAmount() % 5;
			count-=num;
			System.out.println("Dispensing "+num+" 5$ note");
			if(remainder !=0) this.chain.dispense(new Currency(remainder));
		}else{
			
			this.chain.dispense(cur);
			//this.chain.maintainBalance(cur);
		}
	}
	@Override
	public void maintainBalance(Currency cur) {
		if(cur.getAmount()==5)
			count++;
		
			
		
	}
}