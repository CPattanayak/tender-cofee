package com.cofee.common.implimenter;

import com.cofee.common.Currency;
import com.cofee.common.DispenseCurrencyChain;

public class Currency20Dispenser implements DispenseCurrencyChain {

	private DispenseCurrencyChain chain;
	private int count=0;
	
	@Override
	public void setNextChain(DispenseCurrencyChain nextChain) {
		this.chain=nextChain;
	}

	@Override
	public void dispense(Currency cur) {
		if(cur.getAmount() >= 20){
			int num = cur.getAmount()/20;
			int remainder = cur.getAmount() % 20;
			if(num>count) {
				this.chain.dispense(cur);
			}else {
			count-=num;
			System.out.println("Dispensing "+num+" 20 note");
			if(remainder !=0) this.chain.dispense(new Currency(remainder));
			}
		}else{
			
			this.chain.dispense(cur);
			//this.chain.maintainBalance(cur);
		}
	}

	@Override
	public void maintainBalance(Currency cur) {
		if(cur.getAmount() == 20)
			count++;
		else
			this.chain.maintainBalance(cur);
		
	}
}