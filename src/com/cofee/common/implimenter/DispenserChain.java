package com.cofee.common.implimenter;

import com.cofee.common.DispenseCurrencyChain;
import com.cofee.common.ItemPrice;
import com.cofee.common.ItemPriceVisitor;
import com.cofee.common.ItemPriceVisitorImpl;



public class DispenserChain {
	private DispenseCurrencyChain c1;
	private ItemPriceVisitor visitor;
	
	public DispenserChain() {
		this.visitor=new ItemPriceVisitorImpl();
		this.c1 = new Currency20Dispenser();
		DispenseCurrencyChain c2 = new Currency10Dispenser();
		DispenseCurrencyChain c3 = new Currency5Dispenser();

		
		c1.setNextChain(c2);
		c2.setNextChain(c3);
	}
	
	public boolean getAvalableBalance(int[] arr,ItemPrice itemPriceObj) {
		boolean success=true;
		for(int amount :arr) {
			try {
			int dispenceAmount=amount-itemPriceObj.getPrice(this.visitor);
			
			c1.maintainBalance(new com.cofee.common.Currency(amount));
			if(dispenceAmount==0)continue;
			c1.dispense(new com.cofee.common.Currency(dispenceAmount));
			}catch(AvalableBalanceException ex) {
				success=false;
			}
			
		}
		return success;
	
	}

}
