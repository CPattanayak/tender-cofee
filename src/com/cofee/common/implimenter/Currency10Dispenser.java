package com.cofee.common.implimenter;

import com.cofee.common.Currency;
import com.cofee.common.DispenseCurrencyChain;

public class Currency10Dispenser implements DispenseCurrencyChain {

	private DispenseCurrencyChain chain;
	private int count=0;
	
	@Override
	public void setNextChain(DispenseCurrencyChain nextChain) {
		this.chain=nextChain;
	}

	
//	public void dispense(Currency cur) {
//		if(cur.getAmount() >= 10){
//			int num = cur.getAmount()/10;
//			int remainder = cur.getAmount() % 10;
//			System.out.println("Dispensing "+num+" 10 note");
//			if(remainder !=0) this.chain.dispense(new Currency(remainder));
//		}else{
//			this.chain.dispense(cur);
//		}
//	}

	@Override
	public void maintainBalance(Currency cur) {
		if(cur.getAmount()==10)
			count++;
		else
			this.chain.maintainBalance(cur);
			
		
	}


	
	public void dispense(Currency cur) {
		if(cur.getAmount() >= 10){
		int num = cur.getAmount()/10;
		if(num >count) {
			this.chain.dispense(cur);
		}else {
		count-=num;
		int remainder = cur.getAmount() % 10;
		System.out.println("Dispensing "+num+" 10 note");
		if(remainder !=0) this.chain.dispense(new Currency(remainder));
		}
		}else{
		
		this.chain.dispense(cur);
		//this.chain.maintainBalance(cur);
	}
	}
}
