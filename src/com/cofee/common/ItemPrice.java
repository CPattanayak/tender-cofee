package com.cofee.common;

public interface ItemPrice {
	int getPrice(ItemPriceVisitor visitor);

}
