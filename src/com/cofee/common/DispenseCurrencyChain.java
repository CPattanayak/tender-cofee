package com.cofee.common;

import com.cofee.common.implimenter.AvalableBalanceException;

public interface DispenseCurrencyChain {
     void setNextChain(DispenseCurrencyChain nextChain);
	
	 void dispense(Currency cur);
	 void maintainBalance(Currency cur);

}

