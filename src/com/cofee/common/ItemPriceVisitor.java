package com.cofee.common;

public interface ItemPriceVisitor {
	int getPriceBySkuId(String skuid);

}
