package com.cofee.entity;

import com.cofee.common.ItemEnum;
import com.cofee.common.ItemPrice;
import com.cofee.common.ItemPriceVisitor;

public class CofeeEntity implements ItemPrice {

	@Override
	public int getPrice(ItemPriceVisitor visitor) {
		// TODO Auto-generated method stub
		return visitor.getPriceBySkuId(ItemEnum.COFEE.toString());
	}

	

}
